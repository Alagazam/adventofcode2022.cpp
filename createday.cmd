SET DAY=0%1
SET DAY=%DAY:~-2%
echo %DAY%
mkdir Day%DAY%
copy Day00\Day00.h Day%DAY%\Day%DAY%.h
copy Day00\Day00.cpp Day%DAY%\Day%DAY%.cpp
copy Day00\Day00Test.cpp Day%DAY%\Day%DAY%Test.cpp
copy Day00\Day00.vcxproj Day%DAY%\Day%DAY%.vcxproj
copy Day00\packages.config Day%DAY%\packages.config
cd Day%DAY%
sed -i -b -- 's/Day00/Day%DAY%/g' *
cd ..

curl --output Day%DAY%\Day%DAY%.txt https://adventofcode.com/2022/day/%1/input --cookie "session=%AoCcookie%"