#include "gtest/gtest.h"
#include <chrono>
#include "../AoCUtils/inputReader.h"
#include "Day01.h"

namespace AoC2020
{
    struct Day01Test
    {
        inline static const std::string input{
"1000\n"
"2000\n"
"3000\n"
"\n"
"4000\n"
"\n"
"5000\n"
"6000\n"
"\n"
"7000\n"
"8000\n"
"9000\n"
"\n"
"10000\n"
        };

        inline static const  uint64_t resultA{ 24000 };
        inline static const  uint64_t resultB{ 45000 };
    };
};

TEST(Day01, Day01a)
{
    auto start = std::chrono::system_clock::now();
    std::istringstream	iss(AoC2020::Day01Test::input);
    auto lines = aoc::ReadInput(iss, ' ', true);

    auto result = Day01::Day01a(lines);
    EXPECT_EQ(AoC2020::Day01Test::resultA, result);

    auto time = std::chrono::system_clock::now() - start;
    std::cout << "Day01a : " << result << "   Time: " << std::chrono::duration_cast<std::chrono::microseconds>(time) << std::endl;
}

TEST(Day01, Day01b)
{
    auto start = std::chrono::system_clock::now();
    std::istringstream	iss(AoC2020::Day01Test::input);
    auto lines = aoc::ReadInput(iss, ' ', true);

    auto result = Day01::Day01b(lines);
    EXPECT_EQ(AoC2020::Day01Test::resultB, result);

    auto time = std::chrono::system_clock::now() - start;
    std::cout << "Day01b : " << result << "   Time: " << std::chrono::duration_cast<std::chrono::microseconds>(time) << std::endl;
}
