#pragma once

struct Day01
{
    static uint64_t Day01a(aoc::inputContent input);

    static uint64_t Day01b(aoc::inputContent input);
};
