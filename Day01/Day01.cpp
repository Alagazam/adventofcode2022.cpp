#include "gtest/gtest.h"
#include <string_view>
#include <chrono>
#include "../AoCUtils/inputReader.h"
#include "Day01.h"


uint64_t Day01::Day01a(aoc::inputContent input)
{
    uint64_t    max{ 0 };
    uint64_t    sum{ 0 };
    for (auto line : input)
    {
        if (line.size() == 0)
        {
            if (sum > max)
            {
                max = sum;
            }
            sum = 0;
        }
        else
        {
            sum += std::stoull(line[0]);
        }
    }
    return  max;
}

uint64_t Day01::Day01b(aoc::inputContent input)
{
    std::multiset<uint64_t>    elfes;
    uint64_t    max{ 0 };
    uint64_t    sum{ 0 };
    for (auto line : input)
    {
        if (line.size() == 0)
        {
            elfes.insert(sum);
            sum = 0;
        }
        else
        {
            sum += std::stoull(line[0]);
        }
    }
    auto total = *elfes.crbegin();
    total += *(++elfes.crbegin());
    total += *(++(++elfes.crbegin()));
    return total;
}



int main(int argc, char** argv)
{
    using namespace std::literals;
    const auto runstring{ "run"sv };
    if (argc == 2 && runstring == argv[1])
    {
        auto file = aoc::OpenInputFile("Day01.txt");
        auto lines = aoc::ReadInput(*file, ' ');

        {
            auto start = std::chrono::system_clock::now();
            auto result = Day01::Day01a(lines);

            auto time = std::chrono::system_clock::now() - start;
            std::cout << "Day01a : " << result << "   Time: " << std::chrono::duration_cast<std::chrono::microseconds>(time) << std::endl;
        }
        {
            auto start = std::chrono::system_clock::now();
            auto result = Day01::Day01b(lines);

            auto time = std::chrono::system_clock::now() - start;
            std::cout << "Day01b : " << result << "   Time: " << std::chrono::duration_cast<std::chrono::microseconds>(time) << std::endl;
        }
    }
    else
    {
        ::testing::InitGoogleTest(&argc, argv);
        int val = RUN_ALL_TESTS();
        return val;
    }
}
