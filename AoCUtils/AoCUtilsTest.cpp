#include "gtest/gtest.h"
#include "inputReader.h"

TEST(TokenizerTest, OneToken)
{
	std::string s("Test");
	auto tokens = aoc::Tokenize(s);
	ASSERT_EQ(1, tokens.size());
	EXPECT_EQ("Test", tokens[0]);
}
TEST(TokenizerTest, TwoTokens)
{
	std::string s("Test this");
	auto tokens = aoc::Tokenize(s, ' ');
	ASSERT_EQ(2, tokens.size());
	EXPECT_EQ("this", tokens[1]);
}
TEST(TokenizerTest, Tokenize) {
	auto	input = aoc::Tokenize("1,2,3,4");
	EXPECT_EQ(4, input.size());
	aoc::rowContent	row{ "1","2","3","4" };
	EXPECT_EQ(row, input);
}

TEST(ReadInputTest, TokenizeInt) {
	auto	input = aoc::TokenizeInt("1,2,3,4");

	ASSERT_EQ(4, input.size());

	aoc::rowContentInt	row{ 1,2,3,4 };
	EXPECT_EQ(row, input);
}

TEST(ReadInputTest, ReadInputOneLine)
{
	std::istringstream	iss("This is a Test");
	auto result = aoc::ReadInput(iss, ' ');
	ASSERT_EQ(1, result.size());
	ASSERT_EQ(4, result[0].size());
	EXPECT_EQ("This", result[0][0]);
	EXPECT_EQ("a", result[0][2]);
	EXPECT_EQ("Test", result[0][3]);
}
TEST(ReadInputTest, ReadInputTwoLine)
{
	std::istringstream	iss("This is a Test\nAnd now line 2");
	auto result = aoc::ReadInput(iss, ' ');
	ASSERT_EQ(2, result.size());
	ASSERT_EQ(4, result[1].size());
	EXPECT_EQ("Test", result[0][3]);
	EXPECT_EQ("And", result[1][0]);
	EXPECT_EQ("2", result[1][3]);
}
TEST(ReadInputTest, ReadInputOneLineWithLineBreak)
{
	std::istringstream	iss("This is a Test\n");
	auto result = aoc::ReadInput(iss, ' ', false);
	ASSERT_EQ(1, result.size());
	EXPECT_EQ("Test", result[0][3]);
}
TEST(ReadInputTest, ReadInputOneLineWithLineBreakAddLine)
{
	std::istringstream	iss("This is a Test\n");
	auto result = aoc::ReadInput(iss, ' ', true);
	ASSERT_EQ(2, result.size());
	EXPECT_EQ("Test", result[0][3]);
}
TEST(ReadInputTest, ReadInputFunction) {
	std::istringstream ss("1,2,3,4\n-1,-2,-3\n");
	auto	input = aoc::ReadInput(ss);

	ASSERT_EQ(2, input.size());

	aoc::rowContent	row0{ "1","2","3","4" };
	EXPECT_EQ(row0, input[0]);

	aoc::rowContent	row1{ "-1","-2","-3" };
	EXPECT_EQ(row1, input[1]);
}

TEST(ReadInputTest, ReadInputInt) {
	std::istringstream ss("1,2,3,4\n-1,-2,-3\n");
	auto	input = aoc::ReadInputInt(ss);

	ASSERT_EQ(2, input.size());

	aoc::rowContentInt	row0{ 1,2,3,4 };
	EXPECT_EQ(row0, input[0]);

	aoc::rowContentInt	row1{ -1,-2,-3 };
	EXPECT_EQ(row1, input[1]);
}


TEST(ReadInputLinesTest, ReadInputTwoLine)
{
	std::istringstream	iss("This is a Test\nAnd now line 2");
	auto result = aoc::ReadInputLines(iss);
	ASSERT_EQ(2, result.size());
	EXPECT_EQ("This is a Test", result[0]);
	EXPECT_EQ("And now line 2", result[1]);
}

TEST(OpenInputFile, OpenFileAndRead)
{
	auto file = aoc::OpenInputFile("test.txt");
	auto result = aoc::ReadInput(*file, ' ');
	ASSERT_EQ(2, result.size());
	ASSERT_EQ(4, result[1].size());
	EXPECT_EQ("And", result[1][0]);
	EXPECT_EQ("2", result[1][3]);
}
