#pragma once
#include <vector>
#include <istream>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <iterator>
#include <memory>
#include "gtest/gtest.h"

template<typename T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& value) {
	for (auto val : value)
		os << val << ',';
	return os;
}

namespace aoc
{

	using rowContent = std::vector<std::string>;

	inline rowContent Tokenize(std::string line, char delim = ',')
	{
		std::istringstream iss(line);
		rowContent tokens;
		std::string item;
		while (std::getline(iss, item, delim))
		{
			tokens.push_back(item);
		}
		return tokens;
	}
	using inputContent = std::vector<rowContent>;

	inline inputContent ReadInput(std::istream& s, char delim = ',', bool addemptyline = false)
	{
		std::string	line;
		inputContent	lines;
		while (std::getline(s, line))
		{
			auto tokens = Tokenize(line, delim);
			lines.push_back(tokens);
		}
		if (addemptyline) lines.push_back(aoc::rowContent());
		return lines;
	}
	using rowContentInt = std::vector<int64_t>;

	inline rowContentInt TokenizeInt(std::string line, char delim=',')
	{
		std::istringstream iss(line);
		rowContentInt tokens;
		std::string item;
		while (std::getline(iss, item, delim))
		{
			tokens.push_back(std::stoll(item));
		}
		return tokens;
	}

	using contentInt = std::vector<rowContentInt>;

	inline contentInt ReadInputInt(std::istream& s, char delim = ',')
	{
		std::string	line;
		contentInt	lines;
		while (std::getline(s, line))
		{
			auto tokens = TokenizeInt(line, delim);
			lines.push_back(tokens);
		}
		return lines;
	}

	using inputLines = std::vector<std::string>;

	inline inputLines ReadInputLines(std::istream& s)
	{
		std::string	line;
		inputLines	lines;
		while (std::getline(s, line))
		{
			lines.push_back(line);
		}
		return lines;
	}


	inline std::unique_ptr<std::istream> OpenInputFile(const std::string fileName)
	{
		// Open the File
		std::unique_ptr<std::istream> in = std::make_unique<std::ifstream>(fileName);

		// Check if file is valid
		if (!*in)
		{
			std::cerr << "Cannot open the File : " << fileName << std::endl;
			throw std::runtime_error("Cannot open file");
		}
		return in;
	}
}
