#include "gtest/gtest.h"
#include <string_view>
#include <chrono>
#include "../AoCUtils/inputReader.h"
#include "Day00.h"


uint64_t Day00::Day00a(aoc::inputContent input)
{
    return  0;
}

uint64_t Day00::Day00b(aoc::inputContent input)
{
    return  0;
}



int main(int argc, char** argv)
{
    using namespace std::literals;
    const auto runstring{ "run"sv };
    if (argc == 2 && runstring == argv[1])
    {
        auto file = aoc::OpenInputFile("Day00.txt");
        auto lines = aoc::ReadInput(*file, ' ');

        {
            auto start = std::chrono::system_clock::now();
            auto result = Day00::Day00a(lines);

            auto time = std::chrono::system_clock::now() - start;
            std::cout << "Day00a : " << result << "   Time: " << std::chrono::duration_cast<std::chrono::microseconds>(time) << std::endl;
        }
        {
            auto start = std::chrono::system_clock::now();
            auto result = Day00::Day00b(lines);

            auto time = std::chrono::system_clock::now() - start;
            std::cout << "Day00b : " << result << "   Time: " << std::chrono::duration_cast<std::chrono::microseconds>(time) << std::endl;
        }
    }
    else
    {
        ::testing::InitGoogleTest(&argc, argv);
        int val = RUN_ALL_TESTS();
        return val;
    }
}
