#include "gtest/gtest.h"
#include <chrono>
#include "../AoCUtils/inputReader.h"
#include "Day00.h"

namespace AoC2020
{
    struct Day00Test
    {
        inline static const std::string input{
            ""
        };

        inline static const  uint64_t resultA{ 0 };
        inline static const  uint64_t resultB{ 0 };
    };
};

TEST(Day00, Day00a)
{
    auto start = std::chrono::system_clock::now();
    std::istringstream	iss(AoC2020::Day00Test::input);
    auto lines = aoc::ReadInput(iss, ' ');

    auto result = Day00::Day00a(lines);
    EXPECT_EQ(AoC2020::Day00Test::resultA, result);

    auto time = std::chrono::system_clock::now() - start;
    std::cout << "Day00a : " << result << "   Time: " << std::chrono::duration_cast<std::chrono::microseconds>(time) << std::endl;
}

TEST(Day00, Day00b)
{
    auto start = std::chrono::system_clock::now();
    std::istringstream	iss(AoC2020::Day00Test::input);
    auto lines = aoc::ReadInput(iss, ' ');

    auto result = Day00::Day00b(lines);
    EXPECT_EQ(AoC2020::Day00Test::resultB, result);

    auto time = std::chrono::system_clock::now() - start;
    std::cout << "Day00b : " << result << "   Time: " << std::chrono::duration_cast<std::chrono::microseconds>(time) << std::endl;
}
