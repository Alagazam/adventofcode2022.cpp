#pragma once

struct Day00
{
    static uint64_t Day00a(aoc::inputContent input);

    static uint64_t Day00b(aoc::inputContent input);
};
